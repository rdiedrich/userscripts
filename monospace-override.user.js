// ==UserScript==
// @name        Override Monospaced Font Styles
// @namespace   Violentmonkey Scripts
// @match       *://*/*
// @noframes
// @grant       none
// @version     1.2
// @author      Rüdiger Diedrich
// @description 5/27/2020, 8:27:19 AM
// ==/UserScript==

;(() => {
  const addCSSRule = (rule) => {
    let style = document.createElement('style');
    style.appendChild(document.createTextNode(rule));
    document.head.appendChild(style);
  };

  const fontFamily = 'Input Mono';
  const selectors = [
    '.blob-code-inner', // Github
    '.file-content.code pre code', // Gitlab
    'pre', 'code', 'kbd', 'samp'
  ];
  addCSSRule(`
${selectors.join()} {
  font-family: ${fontFamily} !important;
  font-size: 12pt !important;
}`);
})();
